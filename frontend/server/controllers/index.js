const googleMaps = require('./googleMaps/index.js');
const truck = require('./truck/index.js');
const driver = require('./driver/index.js');
const travel = require('./travel/index.js');

module.exports = express => {
  const router = express.Router();

  // GOOGLE MAPS
  router.post('/api/v1/places/', googleMaps.getNearbyPointsOfInterest);

  // TRUCK
  router.get('/api/v1/truck/', truck.getAllTrucks);

  // DRIVER
  router.get('/api/v1/driver/', driver.getAllDrivers);

  // TRAVEL
  router.get('/api/v1/travel/', travel.getAllTravels);
  router.get('/api/v1/travel/:id', travel.getTravel);

  return router;
};
