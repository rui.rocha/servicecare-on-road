const fetch = require('isomorphic-fetch');

const host = process.env.BACKEND_URL;
const port = process.env.BACKEND_PORT;

module.exports.getAllTravels = (req, res) =>
  fetch(`${host}:${port}/api/v1/travel`)
    .then(response => response.json().then(data => res.send(data)))
    .catch(error => error);

module.exports.getTravel = ({ params: { id } }, res) =>
  fetch(`${host}:${port}/api/v1/travel/${id}`)
    .then(response => response.json().then(data => res.send(data)))
    .catch(error => error);
