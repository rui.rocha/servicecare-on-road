const fetch = require('isomorphic-fetch');

const googleApiUrl = process.env.GOOGLE_MAPS_API_URL;
const googleMapsApiKey = process.env.GOOGLE_MAPS_API_KEY;

module.exports.getNearbyPointsOfInterest = (
  {
    body: {
      location: { lat, lng },
      radius,
      type,
    },
  },
  res,
) =>
  fetch(
    // eslint-disable-next-line max-len
    `${googleApiUrl}/place/nearbysearch/json?location=${lat},${lng}&radius=${radius}&type=${type}&key=${googleMapsApiKey}`,
  )
    .then(response => response.json().then(data => res.send(data)))
    .catch(error => error);
