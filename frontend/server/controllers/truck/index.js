const fetch = require('isomorphic-fetch');

const host = process.env.BACKEND_URL;
const port = process.env.BACKEND_PORT;

module.exports.getAllTrucks = (req, res) =>
  fetch(`${host}:${port}/api/v1/truck`)
    .then(response => response.json().then(data => res.send(data)))
    .catch(error => error);
