require('dotenv').config();

const webpack = require('webpack');
const withOptimizedImages = require('next-optimized-images');
const withSourceMaps = require('@zeit/next-source-maps');

module.exports = withOptimizedImages(
  withSourceMaps({
    distDir: 'static/dist',
    serverRuntimeConfig: {
      apiUrl: process.env.SITE_URL,
      googleMapsApiURL: process.env.GOOGLE_MAPS_API_URL,
      googleMapsApiKey: process.env.GOOGLE_MAPS_API_KEY,
      productionEnv: process.env.NODE_ENV === 'production',
    },
    publicRuntimeConfig: {
      apiUrl: process.env.SITE_URL,
      googleMapsApiURL: process.env.GOOGLE_MAPS_API_URL,
      googleMapsApiKey: process.env.GOOGLE_MAPS_API_KEY,
      productionEnv: process.env.NODE_ENV === 'production',
    },
    webpack(config, options) {
      const { isServer } = options;

      if (!isServer) {
        const envVariables = {};
        Object.keys(process.env)
          .filter(key => /REACT_APP_/.test(key))
          .forEach(key => {
            envVariables[key] = process.env[key];
          });
        config.plugins.push(new webpack.EnvironmentPlugin(envVariables));
      }

      config.module.rules.push({
        test: /\.(png|jpe?g|gif|svg)(\?raw)$/,
        use: 'raw-loader',
      });

      return config;
    },
  }),
);
