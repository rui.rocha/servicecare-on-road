require('dotenv').config();

const assert = require('assert');

const validEnvironments = ['development', 'test', 'staging', 'production'];

assert(
  validEnvironments.includes(process.env.NODE_ENV),
  `Expecting NODE_ENV to be one of: ${validEnvironments.join(', ')}`,
);
assert(process.env.SITE_URL != null, 'Expecting SITE_URL environment variable to be defined');
assert(process.env.HOST != null, 'Expecting HOST environment variable to be defined');
assert(process.env.PORT != null, 'Expecting PORT environment variable to be defined');
assert(
  process.env.REACT_APP_API_URL != null,
  'Expecting REACT_APP_API_URL environment variable to be defined',
);

const defaultConfig = {
  app: {
    site_url: process.env.SITE_URL,
    api_url: process.env.REACT_APP_API_URL,
    csp: {
      directives: {
        'connect-src': ["'self'", 'https://maps.googleapis.com/maps/api/'],
        'manifest-src': ["'self'"],
        'script-src': [
          "'self'",
          "'unsafe-inline'",
          "'unsafe-eval'",
          'https://cdnjs.cloudflare.com/',
          'https://maps.googleapis.com/maps/',
          'https://maps.googleapis.com/maps-api-v3/',
        ],
        'style-src': [
          "'self'",
          "'unsafe-inline'",
          'https://fonts.googleapis.com',
          'tagmanager.google.com',
          '*.designmynight.com',
        ],
        'font-src': ["'self'", 'fonts.gstatic.com', 'https://fonts.googleapis.com'],
        'object-src': ["'self'"],
        'block-all-mixed-content': true,
        'frame-ancestors': ["'none'"],
      },
    },
  },
  define: {
    underscored: false,
  },
};

// HMR requires unsafe-eval in development mode
if (process.env.NODE_ENV !== 'production') {
  defaultConfig.app.csp.directives['script-src'].push("'unsafe-eval'");
}

module.exports = defaultConfig;
