/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/forbid-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import getConfig from 'next/config';
import { connect } from 'react-redux';
import { first, isEmpty } from 'lodash';
import seoConfig from '~/config/seo';
import Page from '~/components/Page';
import GoogleMapsWrapper from '~/components/GoogleMapsWrapper';
import GoogleMapsOverlay from '~/components/GoogleMapsOverlay';
import Info from '~/components/Info';
import gasStationsActions from '~/actions/gasStations';
import hotelsActions from '~/actions/hotels';
import restaurantActions from '~/actions/restaurants';
import trucksActions from '~/actions/trucks';
import travelActions from '~/actions/travel';
import travelsActions from '~/actions/travels';
import Marker from '~/components/Marker';
import FormContainer from '~/components/FormContainer';
import fields from '~/config/truckForm';
import validateInput from '~/config/truckForm/validator';
import { formValidator } from '~/utils/forms';

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();
const config = process.browser ? publicRuntimeConfig : serverRuntimeConfig;

const { googleMapsApiKey } = config;

class Index extends Component {
  static async getInitialProps() {
    return {
      meta: seoConfig.index,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      mapInstance: {},
      mapApi: {},
      selectedPOI: {},
      showSelectedPOI: false,
      distanceInfo: {
        value: 0,
        lat: null,
        lng: null,
      },
      errors: {},
    };
  }

  apiHasLoaded = ({ map, maps }) =>
    this.setState({
      mapInstance: map,
      mapApi: maps,
    });

  drawTravelInfo = () => {
    const {
      gasStations,
      hotels,
      restaurants,
      travel: {
        currentPoint: { lat, lng },
      },
    } = this.props;

    const { mapInstance, mapApi } = this.state;

    const coordinates = { lat: parseFloat(lat), lng: parseFloat(lng) };

    const location = new mapApi.LatLng(coordinates.lat, coordinates.lng);

    const allPlaces = [].concat(restaurants, gasStations, hotels);

    const nearestPlace = allPlaces.reduce((prev, curr) => {
      const prevPosition = new mapApi.LatLng(
        prev.geometry.location.lat,
        prev.geometry.location.lng,
      );

      const currPosition = new mapApi.LatLng(
        curr.geometry.location.lat,
        curr.geometry.location.lng,
      );

      const cpos = mapApi.geometry.spherical.computeDistanceBetween(location, currPosition);
      const ppos = mapApi.geometry.spherical.computeDistanceBetween(location, prevPosition);
      return cpos > ppos ? prev : curr;
    });

    const distanceRemaining = mapApi.geometry.spherical.computeDistanceBetween(
      location,
      new mapApi.LatLng(nearestPlace.geometry.location.lat, nearestPlace.geometry.location.lng),
    );

    const lineCoordinates = [coordinates, nearestPlace.geometry.location];

    const linePath = new mapApi.Polyline({
      path: lineCoordinates,
      geodesic: true,
      strokeColor: '#30a9b5',
    });

    linePath.setMap(mapInstance);

    const anchorPoint = mapApi.geometry.spherical.interpolate(
      location,
      new mapApi.LatLng(nearestPlace.geometry.location.lat, nearestPlace.geometry.location.lng),
      0.8,
    );

    this.setState({
      distanceInfo: {
        value: distanceRemaining,
        lat: anchorPoint.lat(),
        lng: anchorPoint.lng(),
      },
    });

    // Zoom to the current point and nearest POI
    const bounds = new mapApi.LatLngBounds();

    bounds.extend(location);
    bounds.extend(
      new mapApi.LatLng(nearestPlace.geometry.location.lat, nearestPlace.geometry.location.lng),
    );

    mapInstance.fitBounds(bounds);
  };

  onChildClick = key => {
    const { gasStations, hotels, restaurants } = this.props;

    const allPlaces = [].concat(restaurants, gasStations, hotels);

    this.setState(() => {
      const selectedPOI = first(allPlaces.filter(e => e.id === key));

      return { selectedPOI, showSelectedPOI: true };
    });
  };

  showSelectedPOI = () => {
    const { showSelectedPOI } = this.state;

    this.setState({
      showSelectedPOI: !showSelectedPOI,
    });
  };

  handleSubmitFilter = event => {
    event.preventDefault();

    const { name, poiType, radius } = event.target.elements;

    const { isValid, errors } = formValidator(
      {
        name,
        poiType,
        radius,
      },
      validateInput,
    );
    if (isValid) {
      const {
        getAllTrucks,
        getAllTravels,
        resetAllGasStations,
        resetAllHotels,
        resetAllRestaurants,
      } = this.props;

      // Example
      const selectedPoiType = poiType.options[poiType.value].text;
      const selectedRadius = parseInt(radius.options[radius.value].text, 10);
      Promise.all([
        getAllTrucks(),
        getAllTravels(),
        resetAllGasStations(),
        resetAllHotels(),
        resetAllRestaurants(),
      ]).then(() => {
        const {
          trucks,
          travels,
          getTravel,
          getNearbyGasStations,
          getNearbyHotels,
          getNearbyRestaurants,
        } = this.props;

        const truck = first(trucks.filter(({ plate }) => plate === name.value));

        if (truck) {
          const travel = first(travels.filter(({ truckId }) => truckId === truck.id));

          getTravel({ id: travel.travelId }).then(() => {
            const {
              travel: { currentPoint },
            } = this.props;

            const coordinates = currentPoint;

            const poiToSearch = [];

            switch (selectedPoiType) {
              case 'Gas Stations':
                poiToSearch.push(
                  getNearbyGasStations({
                    coordinates,
                    type: selectedPoiType,
                    radius: selectedRadius,
                  }),
                );
                break;
              case 'Hotels':
                poiToSearch.push(
                  getNearbyHotels({
                    coordinates,
                    type: selectedPoiType,
                    radius: selectedRadius,
                  }),
                );
                break;
              case 'Restaurants':
                poiToSearch.push(
                  getNearbyRestaurants({
                    coordinates,
                    type: selectedPoiType,
                    radius: selectedRadius,
                  }),
                );
                break;
              default:
                poiToSearch.push(
                  getNearbyGasStations({
                    coordinates,
                    type: selectedPoiType,
                    radius: selectedRadius,
                  }),
                  getNearbyHotels({
                    coordinates,
                    type: selectedPoiType,
                    radius: selectedRadius,
                  }),
                  getNearbyRestaurants({
                    coordinates,
                    type: selectedPoiType,
                    radius: selectedRadius,
                  }),
                );
            }

            Promise.all(poiToSearch).then(() => {
              this.drawTravelInfo();
            });
          });
        }
      });
    } else {
      this.setState({
        errors,
      });
    }
  };

  render() {
    const {
      gasStations,
      hotels,
      restaurants,
      travel: { travelId, startPoint, currentPoint, pointsTravelled },
    } = this.props;

    const { showSelectedPOI, selectedPOI, distanceInfo, errors } = this.state;

    const defaultCenter = { lat: 41.5301693, lng: -8.6123109 };
    const defaultZoom = 18;
    const submitLabel = 'Apply';

    return (
      <Page {...this.props}>
        <Info
          data={selectedPOI || {}}
          checked={showSelectedPOI}
          closeCallback={this.showSelectedPOI}
        />
        <GoogleMapsOverlay>
          <FormContainer
            id="filter-truck-settings"
            submitLabel={submitLabel}
            fields={fields}
            handleSubmit={this.handleSubmitFilter}
            handleErrors={validateInput}
            errors={errors}
          />
        </GoogleMapsOverlay>
        <GoogleMapsWrapper
          apiKey={googleMapsApiKey}
          defaultCenter={defaultCenter}
          defaultZoom={defaultZoom}
          libraries={['places', 'geometry']}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={this.apiHasLoaded}
          onChildClick={this.onChildClick}
        >
          {gasStations.map(place => (
            <Marker
              key={place.id}
              text={place.name}
              lat={place.geometry.location.lat}
              lng={place.geometry.location.lng}
              type="gas-station"
            />
          ))}
          {hotels.map(place => (
            <Marker
              key={place.id}
              text={place.name}
              lat={place.geometry.location.lat}
              lng={place.geometry.location.lng}
              type="hotel"
            />
          ))}
          {restaurants.map(place => (
            <Marker
              key={place.id}
              text={place.name}
              lat={place.geometry.location.lat}
              lng={place.geometry.location.lng}
              type="restaurant"
            />
          ))}
          {!isEmpty(startPoint) && (
            <Marker
              text={`start-${travelId}`}
              lat={startPoint.lat}
              lng={startPoint.lng}
              type="start-location"
            />
          )}
          {!isEmpty(currentPoint) && (
            <Marker
              text={`current-${travelId}`}
              lat={currentPoint.lat}
              lng={currentPoint.lng}
              type="current-location"
            />
          )}
          {!isEmpty(pointsTravelled) &&
            pointsTravelled.map((path, index) => (
              <Marker
                key={path.id}
                text={`current-${travelId}-${index}`}
                lat={path.lat}
                lng={path.lng}
                type="path-location"
              />
            ))}
          {distanceInfo.value > 0 && (
            <Marker
              text={`distance-${travelId}`}
              lat={distanceInfo.lat}
              lng={distanceInfo.lng}
              value={distanceInfo.value}
              type="distance"
            />
          )}
        </GoogleMapsWrapper>
      </Page>
    );
  }
}

const mapStateToProps = ({ gasStations, hotels, restaurants, trucks, travel, travels }) => ({
  gasStations,
  hotels,
  restaurants,
  trucks,
  travel,
  travels,
});

const mapDispatchToProps = dispatch => ({
  getAllTrucks: () => dispatch(trucksActions.getAllTrucks()),
  getAllTravels: () => dispatch(travelsActions.getAllTravels()),
  getNearbyGasStations: params => dispatch(gasStationsActions.getNearbyGasStations(params)),
  getNearbyHotels: params => dispatch(hotelsActions.getNearbyHotels(params)),
  getNearbyRestaurants: params => dispatch(restaurantActions.getNearbyRestaurants(params)),
  getTravel: params => dispatch(travelActions.getTravel(params)),
  resetAllGasStations: () => dispatch(gasStationsActions.resetAllGasStations()),
  resetAllHotels: () => dispatch(hotelsActions.resetAllHotels()),
  resetAllRestaurants: () => dispatch(restaurantActions.resetAllRestaurants()),
});

Index.propTypes = {
  gasStations: PropTypes.array.isRequired,
  hotels: PropTypes.array.isRequired,
  restaurants: PropTypes.array.isRequired,
  getNearbyGasStations: PropTypes.func.isRequired,
  getNearbyHotels: PropTypes.func.isRequired,
  getNearbyRestaurants: PropTypes.func.isRequired,
  resetAllGasStations: PropTypes.func.isRequired,
  resetAllHotels: PropTypes.func.isRequired,
  resetAllRestaurants: PropTypes.func.isRequired,
  travel: PropTypes.shape({
    travelId: PropTypes.string.isRequired,
    startPoint: PropTypes.number.isRequired,
    currentPoint: PropTypes.number.isRequired,
    pointsTravelled: PropTypes.number.isRequired,
  }).isRequired,
};

Index.defaultProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
