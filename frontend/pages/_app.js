import React from 'react';
import App from 'next/app';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import makeStore from '~/utils/store';
import variables from '~/shared/styles/variables';
import RebootStyle from '~/shared/styles/reboot';
import GlobalStyle from '~/shared/styles/globals';

class CustomApp extends App {
  static async getInitialProps({ Component, ctx }) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};

    return { pageProps };
  }

  render() {
    const { Component, pageProps, store } = this.props;

    return (
      <Provider store={store}>
        <ThemeProvider theme={[variables].reduce((p, c) => Object.assign(p, c))}>
          <RebootStyle />
          <GlobalStyle theme={variables} />
          <Component {...pageProps} />
        </ThemeProvider>
      </Provider>
    );
  }
}

export default withRedux(makeStore)(CustomApp);
