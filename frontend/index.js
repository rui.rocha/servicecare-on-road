const express = require('express');
const expressValidator = require('express-validator');
const compression = require('compression');
const helmet = require('helmet');
const hpp = require('hpp');
const bodyParser = require('body-parser');
const next = require('next');
const config = require('./config');
const controllers = require('./server/controllers')(express);

const dev = process.env.NODE_ENV !== 'production';
const host = process.env.HOST;
const port = process.env.PORT;
const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: false }));
    server.use(hpp());
    server.use(helmet());
    server.use(helmet.contentSecurityPolicy(config.app.csp));
    server.use(expressValidator());
    server.use(compression({ threshold: 0 }));
    server.use(express.static('public'));

    server.use('/', controllers);
    server.get('*', (req, res) => {
      handle(req, res);
    });

    server.listen(port, host, err => {
      if (err) throw err;
      console.log(`> Ready on ${host}:${process.env.PORT}`);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
