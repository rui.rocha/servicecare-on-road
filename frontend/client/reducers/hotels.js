import types from '~/actions/types';
import initialState from './initialState';

export default (state = initialState.hotels, action) => {
  switch (action.type) {
    case types.GET_NEARBY_HOTELS:
      return action.payload.results;
    case types.RESET_HOTELS:
      return initialState.hotels;
    default:
      return state;
  }
};
