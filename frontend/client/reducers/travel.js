import first from 'lodash/first';
import types from '~/actions/types';
import initialState from './initialState';

function getCoordinate(str) {
  const aux = str.replace(/[(){}"]/g, '').split(',');

  const coordinates = [];

  for (let i = 0; i < aux.length; i += 2) {
    coordinates.push({ lat: aux[i], lng: aux[i + 1] });
  }

  return coordinates;
}

export default (state = initialState.travel, action) => {
  switch (action.type) {
    case types.GET_TRAVEL: {
      const {
        currentPoint,
        pointsTravelled,
        travelId,
        driverId,
        endPoint,
        startPoint,
        truckId,
      } = action.payload;

      return {
        travelId,
        truckId,
        driverId,
        currentPoint: first(getCoordinate(currentPoint)),
        pointsTravelled: getCoordinate(pointsTravelled),
        endPoint: first(getCoordinate(endPoint)),
        startPoint: first(getCoordinate(startPoint)),
      };
    }
    default:
      return state;
  }
};
