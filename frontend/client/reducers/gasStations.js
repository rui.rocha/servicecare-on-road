import types from '~/actions/types';
import initialState from './initialState';

export default (state = initialState.gasStations, action) => {
  switch (action.type) {
    case types.GET_NEARBY_GAS_STATIONS:
      return action.payload.results;
    case types.RESET_GAS_STATIONS:
      return initialState.gasStations;
    default:
      return state;
  }
};
