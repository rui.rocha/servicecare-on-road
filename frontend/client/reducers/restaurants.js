import types from '~/actions/types';
import initialState from './initialState';

export default (state = initialState.restaurants, action) => {
  switch (action.type) {
    case types.GET_NEARBY_RESTAURANTS:
      return action.payload.results;
    case types.RESET_RESTAURANTS:
      return initialState.restaurants;
    default:
      return state;
  }
};
