import { combineReducers } from 'redux';
import hotels from './hotels';
import restaurants from './restaurants';
import gasStations from './gasStations';
import trucks from './trucks';
import travel from './travel';
import travels from './travels';

export default combineReducers({
  gasStations,
  hotels,
  restaurants,
  trucks,
  travel,
  travels,
});
