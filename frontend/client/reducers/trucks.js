import types from '~/actions/types';
import initialState from './initialState';

export default (state = initialState.trucks, action) => {
  switch (action.type) {
    case types.GET_ALL_TRUCKS:
      return action.payload;
    default:
      return state;
  }
};
