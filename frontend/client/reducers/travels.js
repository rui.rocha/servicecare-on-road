import types from '~/actions/types';
import initialState from './initialState';

export default (state = initialState.travels, action) => {
  switch (action.type) {
    case types.GET_ALL_TRAVELS:
      return action.payload;
    default:
      return state;
  }
};
