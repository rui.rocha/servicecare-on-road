import fetch from 'isomorphic-fetch';
import { apiRoutes } from '~/config/routes';

class TravelsApi {
  static getAllTravels() {
    return fetch(apiRoutes.GET_ALL_TRAVELS(), {
      method: 'GET',
    })
      .then(res => res.json())
      .catch(error => error);
  }
}

export default TravelsApi;
