import fetch from 'isomorphic-fetch';
import { apiRoutes } from '~/config/routes';

class PointsOfInterestApi {
  static getTravel({ id }) {
    return fetch(apiRoutes.GET_TRAVEL(id), {
      method: 'GET',
    })
      .then(res => res.json())
      .catch(error => error);
  }
}

export default PointsOfInterestApi;
