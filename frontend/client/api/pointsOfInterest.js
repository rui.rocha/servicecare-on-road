import fetch from 'isomorphic-fetch';
import { apiRoutes } from '~/config/routes';

class PointsOfInterestApi {
  static getNearbyPointsOfInterest({ coordinates, radius, type }) {
    return fetch(apiRoutes.GET_NEARBY_POINTS_OF_INTEREST(), {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        location: coordinates,
        radius,
        type,
      }),
    })
      .then(res => res.json())
      .catch(error => error);
  }
}

export default PointsOfInterestApi;
