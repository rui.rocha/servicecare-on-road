import fetch from 'isomorphic-fetch';
import { apiRoutes } from '~/config/routes';

class TrucksApi {
  static getAllTrucks() {
    return fetch(apiRoutes.GET_ALL_TRUCKS(), {
      method: 'GET',
    })
      .then(res => res.json())
      .catch(error => error);
  }
}

export default TrucksApi;
