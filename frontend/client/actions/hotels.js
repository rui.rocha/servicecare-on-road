import PointsOfInterestAPI from '~/api/pointsOfInterest';
import types from '~/actions/types';

const Hotels = {
  getNearbyHotels(params) {
    return dispatch =>
      PointsOfInterestAPI.getNearbyPointsOfInterest({ ...params, type: 'lodging' }).then(res =>
        dispatch({ type: types.GET_NEARBY_HOTELS, payload: res }),
      );
  },
  resetAllHotels() {
    return dispatch => dispatch({ type: types.RESET_HOTELS });
  },
};

export default Hotels;
