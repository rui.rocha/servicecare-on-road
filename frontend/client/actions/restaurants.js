import PointsOfInterestAPI from '~/api/pointsOfInterest';
import types from '~/actions/types';

const Restaurants = {
  getNearbyRestaurants(params) {
    return dispatch =>
      PointsOfInterestAPI.getNearbyPointsOfInterest({ ...params, type: 'restaurant' }).then(res =>
        dispatch({ type: types.GET_NEARBY_RESTAURANTS, payload: res }),
      );
  },
  resetAllRestaurants() {
    return dispatch => dispatch({ type: types.RESET_RESTAURANTS });
  },
};

export default Restaurants;
