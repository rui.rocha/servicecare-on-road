import TravelsApi from '~/api/travels';
import types from '~/actions/types';

const Travels = {
  getAllTravels() {
    return dispatch =>
      TravelsApi.getAllTravels().then(res =>
        dispatch({ type: types.GET_ALL_TRAVELS, payload: res }),
      );
  },
};

export default Travels;
