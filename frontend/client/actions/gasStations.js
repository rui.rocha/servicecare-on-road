import PointsOfInterestAPI from '~/api/pointsOfInterest';
import types from '~/actions/types';

const GasStations = {
  getNearbyGasStations(params) {
    return dispatch =>
      PointsOfInterestAPI.getNearbyPointsOfInterest({ ...params, type: 'gas_stations' }).then(res =>
        dispatch({ type: types.GET_NEARBY_GAS_STATIONS, payload: res }),
      );
  },
  resetAllGasStations() {
    return dispatch => dispatch({ type: types.RESET_GAS_STATIONS });
  },
};

export default GasStations;
