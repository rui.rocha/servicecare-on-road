import TravelInfoAPI from '~/api/travel';
import types from '~/actions/types';

const Travel = {
  getTravel(params) {
    return dispatch =>
      TravelInfoAPI.getTravel(params).then(res =>
        dispatch({ type: types.GET_TRAVEL, payload: res }),
      );
  },
};

export default Travel;
