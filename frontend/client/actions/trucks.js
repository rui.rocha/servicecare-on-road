import TrucksApi from '~/api/trucks';
import types from '~/actions/types';

const Trucks = {
  getAllTrucks() {
    return dispatch =>
      TrucksApi.getAllTrucks().then(res => dispatch({ type: types.GET_ALL_TRUCKS, payload: res }));
  },
};

export default Trucks;
