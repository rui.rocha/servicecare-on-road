/**
 /* Apply parameters for each request:
 /* content-type, authorization type, and body
*/
export default ({ method, body }) => {
  const request = {
    method,
    headers: {
      'Content-Type': 'application/json',
    },
  };

  if (body) {
    Object.assign(request, { body: JSON.stringify(body) });
  }

  return request;
};
