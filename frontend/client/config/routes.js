/* eslint-disable max-len */
import getConfig from 'next/config';

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();
const config = process.browser ? publicRuntimeConfig : serverRuntimeConfig;

const { apiUrl } = config;

export const apiRoutes = {
  GET_NEARBY_POINTS_OF_INTEREST: () => `${apiUrl}/api/v1/places`,
  GET_ALL_TRUCKS: () => `${apiUrl}/api/v1/truck`,
  GET_TRAVEL: id => `${apiUrl}/api/v1/travel/${id}`,
  GET_ALL_TRAVELS: () => `${apiUrl}/api/v1/travel`,
};

export const pageRoutes = {
  HOME_PATH: '/',
};
