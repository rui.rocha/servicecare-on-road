import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';
import errorList from './errors';

export default function(data) {
  const { name, value } = data;
  const error = {};

  switch (name) {
    case 'plate':
      if (!value || Validator.isEmpty(value)) {
        error.message = errorList.plateEmptyValue;
      }
      break;
    default:
      break;
  }

  return {
    error: error.message,
    isValid: isEmpty(error),
  };
}
