export default {
  name: {
    type: 'text',
    value: '',
    isDirty: false,
    placeholder: 'Search by license plate',
  },
  poiType: {
    isDirty: false,
    type: 'select',
    placeholder: 'Select POI type',
    value: [
      {
        type: 'select',
        value: [
          {
            id: 1,
            value: 'View All',
          },
          {
            id: 2,
            value: 'Gas Stations',
          },
          {
            id: 3,
            value: 'Restaurants',
          },
          {
            id: 4,
            value: 'Hotels',
          },
        ],
        isDirty: false,
      },
    ],
  },
  radius: {
    isDirty: false,
    type: 'select',
    placeholder: 'Select radius',
    value: [
      {
        type: 'select',
        value: [
          {
            id: 1,
            value: '500',
          },
          {
            id: 2,
            value: '1500',
          },
          {
            id: 3,
            value: '3000',
          },
          {
            id: 4,
            value: '6000',
          },
        ],
        isDirty: false,
      },
    ],
  },
};
