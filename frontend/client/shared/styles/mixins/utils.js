export default {
  rem(fontSize, browserContext = 16) {
    return `${fontSize / browserContext}rem`;
  },
  hide() {
    return `
      display: none !important;
    `;
  },
  show() {
    return `
      display: block !important;
    `;
  },
};
