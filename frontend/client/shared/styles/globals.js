import { createGlobalStyle } from 'styled-components';
import { media } from '~/shared/styles/mixins/media';

export default createGlobalStyle`
  /*
  ==========================================================================
    Document
  ==========================================================================

  1. Stretch <html> stretch to fill our screen height
  2. Make children of html (body) occupy at least 100% of the screen
  3. Viewport is scalable and occupies at least 375px (iPhone 6)
  */

  html {
    max-width: 100vw;
    min-width: 0;
    height: 0;
    min-height: 100%;
    display: flex;
    flex-direction: column;
    background-color: ${props => props.theme.colors.background};
    box-sizing: border-box;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  /*
  ==========================================================================
    Body
  ==========================================================================

  1. Force scroll always to prevent scrollbars to appear/disappear based on the page contents
  2. Make sure that we occupy 100% of our parent and allow our child elements to do the same
  3. Needed for IE11 otherwise flex wouldn't grow vertically,
  see https://stackoverflow.com/a/42930574
  */

  body {
    max-width: 100vw;
    overflow-x: hidden !important;
    display: flex;
    flex: 1 0 auto;
    overflow-y: scroll;
    margin: 0;
    flex-direction: column;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: ${props => props.theme.typography.browserContext};
    font-weight: 400;
    font-style: normal;
    font-display: swap;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    text-rendering: optimizeLegibility;
    overflow-wrap: break-word; /* Break long words by default */

    > div {
      > div {
        display: flex;
      }
    }
  }

  p {
    font-family: 'Source Sans Pro', sans-serif;
    font-size: ${props => props.theme.typography.xs.paragraph};
    line-height: ${props => props.theme.typography.xs.paragraphLineHeight};

    ${media.md`
      font-size: ${props => props.theme.typography.md.paragraph};
      line-height: ${props => props.theme.typography.md.paragraphLineHeight};
    `};
  }



  /* ==========================================================================
    Anchors
  ========================================================================== */

  a,
  a:focus,
  a:visited,
  a:hover,
  a:active {
    display: inline-block;
    outline: none !important;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: ${props => props.theme.typography.xs.hyperlink};
    color: ${props => props.theme.colors.white};
    line-height: ${props => props.theme.utils.rem(20)};
    text-decoration: none !important;

    ${media.md`
      font-size: ${props => props.theme.typography.md.hyperlink};
    `};
  }

  a[href^="tel"] {
    color: inherit;
  }

  /* ==========================================================================
    Headings
  ========================================================================== */

  h1,
  h2,
  h3,
  h4 {
    margin-top: 0;
    margin-bottom: 0;
    font-style: normal;
    font-weight: 400;
  }

  h1 {
    font-size: ${props => props.theme.typography.xs.h1};
    line-height: ${props => props.theme.typography.xs.h1LineHeight};

    ${media.md`
      font-size: ${props => props.theme.typography.md.h1};
      line-height: ${props => props.theme.typography.md.h1LineHeight} !important;
    `};
  }

  h2 {
    font-size: ${props => props.theme.typography.xs.h2};
    line-height: ${props => props.theme.typography.xs.h2LineHeight};

    ${media.md`
      font-size: ${props => props.theme.typography.md.h2};
      line-height: ${props => props.theme.typography.md.h2LineHeight};
    `};
  }

  h3 {
    font-size: ${props => props.theme.typography.xs.h3};
    line-height: ${props => props.theme.typography.md.h3LineHeight};

    ${media.md`
      font-size: ${props => props.theme.typography.md.h3};
      line-height: ${props => props.theme.typography.md.h3LineHeight};
    `};
  }

  h4 {
    font-size: ${props => props.theme.typography.xs.h4};
    line-height: ${props => props.theme.typography.xs.h4LineHeight};
    font-weight: 500;

    ${media.md`
      font-size: ${props => props.theme.typography.md.h4};
      line-height: ${props => props.theme.typography.md.h4LineHeight};
    `};
  }

  /* ==========================================================================
    Inputs
  ========================================================================== */

  input {
    border-radius: 0;
  }

  select::-ms-expand {
    display: none;
  }

  select:focus option {
    background-color: ${props => props.theme.colors.background};
    color: ${props => props.theme.colors.white};
  }

  input:focus,
  select:focus,
  textarea:focus,
  button:focus {
    outline: none;
  }

  input:not([value=""]) {
    border-color: ${props => props.theme.colors.white};
  }

  input:hover {
    border-color: ${props => props.theme.colors.white};
  }

  @keyframes autofill {
    to {
      background: transparent;
      color: ${props => props.theme.colors.background};
    }
  }

  input:-webkit-autofill {
    animation-name: autofill;
    animation-fill-mode: both;
    -webkit-text-fill-color: ${props => props.theme.colors.grey};
  }

  ::placeholder {
    /* Most modern browsers support this now. */
    color: ${props => props.theme.colors.grey};
  }

`;
