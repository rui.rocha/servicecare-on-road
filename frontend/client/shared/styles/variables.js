import utils from './mixins/utils';

/* ==========================================================================
  Grid
========================================================================== */
const grid = {
  mediaQuery: 'only screen',
  gridSize: {
    xs: 4,
    sm: 4,
    md: 10,
    lg: 10,
  },
  gutterWidth: {
    xs: 10,
    sm: 10,
    md: 25,
    lg: 25,
  },
  outerMargin: {
    xs: 24,
    sm: 24,
    md: 40,
    lg: 40,
  },
  container: {
    xs: 320,
    sm: 660,
    md: 884,
    lg: 1120,
  },
  breakpoints: {
    xs: 0,
    sm: 708,
    md: 964,
    lg: 1200,
  },
};

/* ==========================================================================
  Colors
========================================================================== */
const colors = {
  black: '#000',
  white: '#FFF',
  background: '#FFF',
  grey: '#E2E2E2',
  clearGrey: '#BDBDBD',
  blue: '#30a9b5',
};

/* ==========================================================================
  Typography
========================================================================== */
const mobileTypography = {
  h1: utils.rem(40),
  h1LineHeight: utils.rem(42),
  h2: utils.rem(40),
  h2LineHeight: utils.rem(42),
  h3: utils.rem(24),
  h3LineHeight: utils.rem(25),
  h4: utils.rem(16),
  h4LineHeight: utils.rem(21),
  paragraph: utils.rem(16),
  paragraphLineHeight: utils.rem(20),
  input: utils.rem(16),
  inputLineHeight: utils.rem(20),
  label: utils.rem(14),
};

const tabletDesktopTypography = {
  h1: utils.rem(64),
  h1LineHeight: utils.rem(67),
  h2: utils.rem(40),
  h2LineHeight: utils.rem(47),
  h3: utils.rem(24),
  h3LineHeight: utils.rem(25),
  h4: utils.rem(16),
  h4LineHeight: utils.rem(21),
  paragraph: utils.rem(16),
  paragraphLineHeight: utils.rem(20),
  input: utils.rem(16),
  inputLineHeight: utils.rem(20),
  label: utils.rem(14),
};

const typography = {
  browserContext: utils.rem(16),
  xs: mobileTypography,
  sm: mobileTypography,
  md: tabletDesktopTypography,
  lg: tabletDesktopTypography,
};

/* ==========================================================================
  Layout
========================================================================== */
const layout = {
  none: utils.rem(0),
  micro: utils.rem(8),
  tiny: utils.rem(16),
  small: utils.rem(24),
  medium: utils.rem(40),
  large: utils.rem(64),
  xl: utils.rem(104),
  xxl: utils.rem(168),
  xl3: utils.rem(264),
  xl4: utils.rem(424),
  xl5: utils.rem(680),
  xl6: utils.rem(1088),
};

const mobileGridItem = {
  small: {
    width: '158px',
    height: '158px',
  },
  large: {
    width: '324px',
    height: '324px',
  },
};

const desktopGridItem = {
  small: {
    width: '262px',
    height: '262px',
  },
  large: {
    width: '360px',
    height: '360px',
  },
  landspaceLarge: {
    width: '390px',
    height: '304px',
  },
};

const section = {
  headerPaddingTop: 30,
  stackedWithGridContentPadding: 64,
  navBarHeight: '48px',
  heroSliderInfoActionDescriptionHeight: 20 * 2,
  instaFeedPostDescriptionHeight: 20 * 3,
  intersectedSliderStaticInfoDescriptionHeight: 20 * 3,
  heroContentRow: {
    md: {
      width: '683px',
    },
    lg: {
      width: '732px',
    },
  },
  gridItem: {
    xs: mobileGridItem,
    sm: mobileGridItem,
    md: desktopGridItem,
    lg: desktopGridItem,
  },
};

const pictures = {
  xs: mobileGridItem,
  sm: mobileGridItem,
  md: desktopGridItem,
  lg: desktopGridItem,
};

export default {
  grid,
  colors,
  typography,
  layout,
  section,
  pictures,
  utils,
};
