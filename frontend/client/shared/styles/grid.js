export default {
  flexboxgrid: {
    // Defaults
    gridSize: 10, // columns
    outerMargin: 50,
    mediaQuery: 'only screen',
    container: {
      xs: 320,
      sm: 480,
      md: 720,
      lg: 1400,
    },
    breakpoints: {
      xs: 0,
      sm: 480,
      md: 988,
      lg: 1400,
    },
  },
};
