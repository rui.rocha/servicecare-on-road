import React from 'react';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';
import Container from './styles';

const GoogleMapsWrapper = ({
  apiKey,
  libraries,
  onGoogleApiLoaded,
  defaultCenter,
  defaultZoom,
  yesIWantToUseGoogleMapApiInternals,
  onChildClickCallback,
  onChildClick,
  children,
}) => (
  <Container>
    <GoogleMapReact
      bootstrapURLKeys={{ key: apiKey, libraries }}
      defaultCenter={defaultCenter}
      defaultZoom={defaultZoom}
      yesIWantToUseGoogleMapApiInternals={yesIWantToUseGoogleMapApiInternals}
      onGoogleApiLoaded={onGoogleApiLoaded}
      onChildClickCallback={onChildClickCallback}
      onChildClick={onChildClick}
    >
      {children}
    </GoogleMapReact>
  </Container>
);

GoogleMapsWrapper.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  libraries: PropTypes.array,
  apiKey: PropTypes.string.isRequired,
  defaultCenter: PropTypes.shape({
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
  }).isRequired,
  defaultZoom: PropTypes.number.isRequired,
  onGoogleApiLoaded: PropTypes.func,
  yesIWantToUseGoogleMapApiInternals: PropTypes.bool,
};

GoogleMapsWrapper.defaultProps = {
  libraries: [],
  onGoogleApiLoaded: null,
  yesIWantToUseGoogleMapApiInternals: false,
};

export default GoogleMapsWrapper;
