import React from 'react';
import classNames from 'classnames';
import isEmpty from 'lodash/isEmpty';
import { Container, Content, Navigation, Icon, Title, Address } from './styles';

const Info = ({ data, checked, closeCallback }) => {
  if (isEmpty(data)) {
    return (
      <Container checked={checked} className={checked && 'checked'}>
        <Navigation>
          <div
            className={classNames('icon', checked && 'checked')}
            role="button"
            onClick={closeCallback}
            onKeyDown={closeCallback}
            tabIndex={0}
          >
            +
          </div>
        </Navigation>
        <Content className={classNames(checked && 'checked')}>
          <Title>No Information Available</Title>
        </Content>
      </Container>
    );
  }

  const { icon, name, vicinity } = data;

  return (
    <Container className={checked && 'checked'}>
      <Navigation>
        <div
          className={classNames('icon', checked && 'checked')}
          role="button"
          onClick={closeCallback}
          onKeyDown={closeCallback}
          tabIndex={0}
        >
          +
        </div>
      </Navigation>
      <Content className={classNames(checked && 'checked')}>
        <Icon icon={icon} />
        <Title>{name}</Title>
        <Address>{vicinity}</Address>
      </Content>
    </Container>
  );
};

export default Info;
