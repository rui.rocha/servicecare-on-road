import styled, { keyframes } from 'styled-components';

const reduceFromRight = x => keyframes`
  0% {
    width: ${x};
  }

  50% {
    width: ${x};
  }

  100% {
    width: 0;
  }
`;

// eslint-disable-next-line no-unused-vars
const hideFromRight = (x, y) => keyframes`
  0% {
    margin-left: calc(-6.5rem + 26.5rem)
  }

  50% {
    margin-left: calc(-6.5rem + 26.5rem)
  }

  100% {
    margin-left: calc(${x});
  }
`;

const showContent = keyframes`
  0% {
    opacity: 0;
  }

  50% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
`;

const hideContent = keyframes`
  0% {
    opacity: 1;
  }

  100% {
    opacity: 0;
  }
`;

export const Container = styled.div`
  height: 100vh;
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  display: flex;
  background-image: ${({ background }) => `url(${background})`};
  background-color: ${props => props.theme.colors.white};
  top: 0;
  left: 0;
  width: ${props => props.theme.layout.xl4};
  z-index: 300;

  transition: all 1.5s ease-in-out;

  &.checked {
    width: ${props => props.theme.layout.xl4};
  }

  &:not(.checked) {
    animation: ${props => reduceFromRight(props.theme.layout.xl4)} 2s linear;
    width: 0;
  }
`;

export const Navigation = styled.div`
  height: 100%;
  position: fixed;
  display: flex;
  z-index: 300;

  .icon {
    height: ${props => props.theme.layout.large};
    width: ${props => props.theme.layout.large};
    display: flex;
    background: ${props => props.theme.colors.blue};
    align-self: center;
    justify-content: center;
    align-items: center;
    border-radius: ${props => props.theme.layout.micro};
    cursor: pointer;
    z-index: 400;

    &.checked {
      transition: all 1.4s ease-in-out;

      margin-left: ${props => `calc(-${props.theme.layout.xl} + ${props.theme.layout.xl4})`};
    }

    &:not(.checked) {
      animation: ${props => hideFromRight(`-${props.theme.layout.medium}`, props.theme.layout.xl4)}
        2s linear;
      transition: all 1.6s ease-in-out;
      margin-left: -${props => props.theme.layout.medium};
    }
  }
`;

export const Content = styled.div`
  width: 100%;
  padding: ${props => props.theme.layout.medium} ${props => props.theme.layout.micro};

  > div {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
  }

  &.checked {
    animation: ${showContent} 2s linear;
    opacity: 1;
  }

  &:not(.checked) {
    animation: ${hideContent} 1s linear;
    opacity: 0;
  }
`;

export const Icon = styled.div`
  height: ${props => props.theme.layout.large};
  width: 100%;
  background-image: ${({ icon }) => `url(${icon})`};
  background-size: ${props => props.theme.layout.large};
  background-repeat: no-repeat;
  background-position: center;
`;

export const Title = styled.div`
  padding: ${props => props.theme.layout.small} ${props => props.theme.layout.micro};
`;

export const Address = styled.div`
  padding: ${props => props.theme.layout.small} ${props => props.theme.layout.micro};
`;

export const GoogleLink = styled.a`
  padding: ${props => props.theme.layout.small} ${props => props.theme.layout.micro};
`;
