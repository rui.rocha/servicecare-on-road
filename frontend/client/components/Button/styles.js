import styled from 'styled-components';

export default styled.button`
  height: 100%;
  width: 100%;
  padding: ${props => props.theme.layout.micro} ${props => props.theme.layout.tiny};
  border: none;
  cursor: pointer;
  border-radius: ${props => props.theme.layout.micro};
  background-color: ${props => props.theme.colors.blue};
  color: ${props => props.theme.colors.white};
  font-size: ${props => props.theme.typography.md.input};
  line-height: ${props => props.theme.typography.md.inputLineHeight};
`;
