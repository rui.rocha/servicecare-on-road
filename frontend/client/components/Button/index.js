import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Container from './styles';

const Button = ({ className, type, text, action, dataId, disabled }) => (
  <Container
    id={dataId}
    className={classNames(className)}
    type={type || 'button'}
    onClick={action}
    data-id={dataId}
    disabled={disabled}
  >
    <span>{text}</span>
  </Container>
);

Button.propTypes = {
  className: PropTypes.string.isRequired,
  dataId: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  disabled: false,
};

export default Button;
