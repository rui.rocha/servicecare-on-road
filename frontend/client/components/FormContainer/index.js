/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/forbid-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import merge from 'lodash/merge';
import Form from './Form';

class FormContainer extends Component {
  constructor(props) {
    super(props);
    const { handleSubmit } = props;

    this.state = this.getInitialState();

    this.handleSubmit = handleSubmit.bind(this);
  }

  componentDidUpdate(previousProps) {
    const { errors, fields, reset } = this.props;
    const auxState = this.state;

    if (previousProps.errors !== errors) {
      this.setState(merge({}, auxState, errors));
    }

    if (previousProps.fields !== fields) {
      this.setState(merge({}, auxState, fields));
    }

    if (reset === true && previousProps.reset !== reset) {
      this.setState(this.getInitialState());
    }
  }

  handleChange = event => {
    const { handleChange } = this.props;
    const auxState = this.state;

    if (handleChange) {
      handleChange();
    }

    const { handleErrors } = this.props;
    let { name } = event.target;
    let { value } = event.target;

    if (event.target.type === 'checkbox') {
      value = event.target.checked;
    } else if (event.target.getAttribute('type') === 'selector') {
      name = event.target.getAttribute('name');
      value = event.target.getAttribute('data-id');
    }

    const errors = handleErrors(
      {
        name,
        value,
      },
      auxState,
    );

    this.setState(merge({}, auxState, { [name]: { value, errors } }));
  };

  getInitialState = () => {
    const { fields, defaultValues } = this.props;

    return {
      ...Object.keys(fields).reduce(
        (acc, curr) => ({
          ...acc,
          [curr]: {
            type: fields[curr].type,
            value: defaultValues && defaultValues[curr] ? defaultValues[curr] : fields[curr].value,
            className: fields[curr].className,
            placeholder: fields[curr].placeholder,
            background: fields[curr].background,
            label: fields[curr].label,
            submitIcon: fields[curr].submitIcon,
            config: fields[curr].config,
            errors: { isValid: true },
          },
        }),
        {},
      ),
    };
  };

  render() {
    const { id, className, submitLabel, defaultValues } = this.props;

    return (
      <Form
        id={id}
        className={className}
        submitLabel={submitLabel}
        fields={this.state}
        onChange={this.handleChange}
        onSubmit={this.handleSubmit}
        defaultValues={defaultValues}
      />
    );
  }
}

FormContainer.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  submitLabel: PropTypes.string.isRequired,
  defaultValues: PropTypes.object,
  fields: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleErrors: PropTypes.func.isRequired,
  reset: PropTypes.bool,
};

FormContainer.defaultProps = {
  defaultValues: {},
  reset: false,
};

export default FormContainer;
