/* eslint-disable react/forbid-prop-types */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from '~/components/Button';
import Input from '~/components/Input';
import Select from '~/components/Select';
import Container from './styles';

class Form extends PureComponent {
  static createField(name, field, onChange, defaultValues) {
    const { type } = field;
    const { value, placeholder, label } = field;

    const props = {
      name,
      onChange,
      placeholder,
      value,
      label,
      defaultValues,
    };

    const { className } = field;
    const elements = [];

    switch (type) {
      case 'select': {
        const selectElements = [];

        for (let i = 0; i < value.length; i += 1) {
          selectElements.push(
            <Select
              key={`option-${name}-${i}`}
              name={name}
              data={value[i].value}
              onChange={this.onChangeInput}
              width={value[i].width}
              placeholder={placeholder}
            />,
          );
        }
        elements.push(selectElements);
        break;
      }
      default:
        elements.push(
          <Input key={props.name} className={classNames(className)} type="text" {...props} />,
        );
    }
    return elements;
  }

  constructor(props) {
    super(props);

    this.hasError = false;
  }

  renderElements() {
    const { defaultValues, fields, onChange } = this.props;

    return Object.keys(fields).map(fieldName => {
      const { isValid, error } = fields[fieldName].errors;

      if (this.hasError === false) {
        this.hasError = !isValid && error;
      }

      return Form.createField(fieldName, fields[fieldName], onChange, defaultValues);
    });
  }

  renderSubmitButton() {
    const { submitLabel } = this.props;

    return <Button text={submitLabel} type="submit" />;
  }

  render() {
    const { id, onSubmit } = this.props;

    return (
      <Container>
        <form id={id} onSubmit={onSubmit} noValidate>
          {this.renderElements()}
          {this.renderSubmitButton()}
        </form>
      </Container>
    );
  }
}

Form.propTypes = {
  id: PropTypes.string.isRequired,
  submitLabel: PropTypes.string.isRequired,
  fields: PropTypes.object.isRequired,
  defaultValues: PropTypes.object,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

Form.defaultProps = {
  defaultValues: {},
};

export default Form;
