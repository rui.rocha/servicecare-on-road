import React from 'react';
import PropTypes from 'prop-types';
import Wrapper from './styles';
import startLocationIcon from '~/shared/media/images/icons/icn-first-location.png';
import currentLocationIcon from '~/shared/media/images/icons/icn-current-location.png';
import pathLocationIcon from '~/shared/media/images/icons/icn-path.png';
import gasStationIcon from '~/shared/media/images/icons/icn-gas-station.png';
import hotelIcon from '~/shared/media/images/icons/icn-hotel.png';
import restaurantIcon from '~/shared/media/images/icons/icn-restaurant.png';
// eslint-disable-next-line import/no-unresolved
import distanceRemainingImg from '~/shared/media/images/icons/icn-distance-remaining.svg?include';

const getMarkerIcon = (type, value) => {
  switch (type) {
    case 'start-location':
      return startLocationIcon;
    case 'current-location':
      return currentLocationIcon;
    case 'path-location':
      return pathLocationIcon;
    case 'gas-station':
      return gasStationIcon;
    case 'hotel':
      return hotelIcon;
    case 'restaurant':
      return restaurantIcon;
    case 'distance': {
      const svg = distanceRemainingImg.replace('{{ text }}', `Distance: ${value.toFixed(0)}m`);

      // SVG is repainting too many times? cant match to this use cases below atm
      // .replace('{{ height }}', '100')
      // .replace('{{ width }}', '100')
      // .replace('{{ color }}', '#30a9b5');

      const blob = new Blob([svg], { type: 'image/svg+xml' });
      const blobUrl = URL.createObjectURL(blob);

      return blobUrl;
    }
    default:
      return null;
  }
};

const Marker = ({ type, onClick, value }) => (
  <Wrapper
    alt={type}
    {...(onClick ? { onClick } : {})}
    icon={getMarkerIcon(type, value)}
    square={value}
  />
);

Marker.propTypes = {
  type: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Marker.defaultProps = {
  onClick: null,
  value: null,
};

export default Marker;
