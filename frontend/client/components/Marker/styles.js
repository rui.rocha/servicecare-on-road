import styled, { css } from 'styled-components';

export default styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  height: ${props => props.theme.layout.large};
  width: ${props => props.theme.layout.large};
  background-color: ${props => props.theme.colors.black};
  border-radius: 100%;
  user-select: none;
  transform: translate(-50%, -50%);
  background-image: ${props => `url(${props.icon})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  cursor: pointer;

  &:hover {
    z-index: 100;
  }

  ${p =>
    p.square &&
    css`
      border-radius: 0;
      height: 50px;
      width: 100px;
    `};
`;
