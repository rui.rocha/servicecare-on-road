import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import CustomHead from '~/components/CustomHead';
/* eslint-disable no-unused-vars */
import reboot from '~/shared/styles/reboot';
import globals from '~/shared/styles/globals';
/* eslint-enable no-unused-vars */

class Page extends PureComponent {
  render() {
    const {
      meta: { title, description },
      children,
    } = this.props;

    return (
      <div>
        <CustomHead title={title} description={description} />
        {children}
      </div>
    );
  }
}

Page.propTypes = {
  meta: PropTypes.shape({
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export default Page;
