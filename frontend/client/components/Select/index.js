import React from 'react';
import PropTypes from 'prop-types';
import Svg from '~/components/Svg';
/*  eslint-disable import/no-unresolved */
import arrow from '~/shared/media/images/icons/arrow-select.svg?include';
/*  eslint-enable import/no-unresolved */
import Container from './styles';

const Select = ({ id, name, onChange, data, placeholder, selected, defaultValue }) => {
  const options = [];

  data.forEach(item => {
    options.push(
      <option key={`option-${item.id}`} value={item.id}>
        {item.value}
      </option>,
    );
  });

  return (
    <Container>
      <select
        data-id={id}
        name={name}
        onChange={onChange}
        value={selected}
        defaultValue={defaultValue}
      >
        {placeholder && (
          <option value="" disabled>
            {placeholder}
          </option>
        )}
        {options}
      </select>
      <Svg svg={arrow} />
    </Container>
  );
};

Select.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  data: PropTypes.array,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  selected: PropTypes.string.isRequired,
  defaultValue: PropTypes.string.isRequired,
};

Select.defaultProps = {
  data: [],
};

export default Select;
