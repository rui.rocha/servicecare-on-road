import styled from 'styled-components';
import variables from '~/shared/styles/variables';

const { colors, layout } = variables;

export default styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  cursor: pointer;

  select {
    width: 100%;
    height: 100%;
    appearance: none;
    box-sizing: border-box;
    background-color: transparent;
    padding: 0 ${layout.medium} 0 ${layout.tiny};
    cursor: pointer;
    white-space: nowrap;
    border-radius: ${props => props.theme.layout.micro};
    border: 1px solid ${props => props.theme.colors.clearGrey};

    option {
      color: ${colors.black} !important;
    }
  }

  > div {
    pointer-events: none;

    &:last-child {
      align-self: center;
      margin-left: -${layout.medium};
    }
  }
`;
