import React from 'react';
import PropTypes from 'prop-types';

const Svg = ({ svg }) => (
  <div>
    <i
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{
        __html: svg,
      }}
    />
  </div>
);

Svg.propTypes = {
  svg: PropTypes.string.isRequired,
};

export default Svg;
