import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Container from './styles';

const Input = ({ className, name, type, placeholder, value, onChange }) => (
  <Container>
    <input
      className={classNames(className)}
      name={name}
      type={type || 'text'}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    />
  </Container>
);

Input.propTypes = {
  className: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Input;
