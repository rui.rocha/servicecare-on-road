import styled from 'styled-components';

export default styled.div`
  width: 100%;
  height: 100%;

  input {
    height: 100%;
    width: 100%;
    background: transparent;
    padding-left: ${props => props.theme.layout.tiny};
    box-sizing: border-box;
    border-radius: ${props => props.theme.layout.micro};

    &[type='text'] {
      border: 1px solid ${props => props.theme.colors.clearGrey};
    }
  }
`;
