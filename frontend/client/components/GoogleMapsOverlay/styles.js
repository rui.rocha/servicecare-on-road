import styled from 'styled-components';

export default styled.div`
  position: absolute;
  width: auto;
  top: 0;
  left: 0;
  z-index: 200;
  padding: ${props => props.theme.layout.micro};

  > div {
    padding: 10px;
    background: white;
    width: 65vw;
    height: ${props => props.theme.layout.large};

    form {
      height: 100%;

      > div {
        float: left;
        width: 30%;
        padding: 0 10px;
      }

      > button {
        float: left;
        width: ${props => props.theme.layout.xl};
        margin-left: 10px;
      }
    }
  }
`;
