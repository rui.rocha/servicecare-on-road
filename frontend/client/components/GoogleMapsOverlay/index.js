import React from 'react';
import PropTypes from 'prop-types';
import Container from './styles';

const GoogleMapsOverlay = ({ children }) => <Container>{children}</Container>;

GoogleMapsOverlay.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export default GoogleMapsOverlay;
