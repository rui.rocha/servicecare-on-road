import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

class CustomHead extends PureComponent {
  render() {
    const { title, description } = this.props;
    return (
      <div className="title">
        <Head>
          <title>{title}</title>
          {description ? <meta key="description" name="description" content={description} /> : null}
          {/* any other meta tags */}
        </Head>
      </div>
    );
  }
}

CustomHead.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
};

CustomHead.defaultProps = {
  title: null,
  description: null,
};

export default CustomHead;
