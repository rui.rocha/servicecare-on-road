# Servicecare On Road

The demo of this code challenged is available [here](http://51.178.84.122/).

The auto-generated plate by the ORM to be used in the Search Box is the following:

```sh
123-aaa-345
```

In order to start the project, it is needed to have the Docker dependencies installed.

Once this is done, it is only required to run the command below:

```sh
docker-compose up
```

This will bring up the containers related to backend in Spring Boot, database in Postgres, frontend in NextJS and an Nginx webserver.

On the host, once the containers are up, these should be accessible through `localhost`, with the following ports open to the respective services:

```sh
8080 for the Spring Boot backend
```

```sh
5432 for the Postgres database
```

```sh
3000 for the NextJS frontend (this is accessed by typing  «localhost» alone due to Nginx redirect)
```

```sh
80 for Nginx webserver
```

### .env file

Environment variables defined on `.env` file will be loaded into `process.env`.
Please read [dotenv](https://github.com/motdotla/dotenv) documentation for more information.

Below is an example of how a `.env` file could be:

```
SITE_URL=http://localhost:3000
NAME_URL=http://localhost:3000
HOST=0.0.0.0
PORT=3000
BACKEND_URL=http://localhost
BACKEND_PORT=8080
REACT_APP_API_URL=https://maps.googleapis.com/maps/api
GOOGLE_MAPS_API_URL=https://maps.googleapis.com/maps/api
GOOGLE_MAPS_API_KEY=AIzaSyDUFYcqj4745cQo2y4SQnSjhH1_tifPKxI
NODE_ENV=development

```

## Commands

### start/dev

Starts a development server.

```sh
$ npm run dev
```

Creates a production build.

```sh
$ npm run build
```

Starts the project on the settings defined in the `.env` file with the production build (needs to runs the previous command first).

```sh
$ npm run start
```
