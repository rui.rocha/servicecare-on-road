package com.example.mantruckmonitor.dao;

import com.example.mantruckmonitor.model.Travel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("fakeTravelDao")
public class FakeTravelDataAccessService implements TravelDao {

    private static List<Travel> DB = new ArrayList<>();

    @Override
    public int insertTravel(UUID travel_id, Travel travel) {
        DB.add(new Travel(travel_id, travel.getTruckId(), travel.getDriverId(), travel.getPointsTravelled(), travel.getStartPoint(), travel.getCurrentPoint(), travel.getEndPoint()));

        return 1;
    }

    @Override
    public List<Travel> selectAllTravels() {
        return DB;
    }

    @Override
    public Optional<Travel> selectTravelById(UUID travel_id) {
        return DB.stream()
                .filter(travel -> travel.getTravelId().equals(travel_id))
                .findFirst();
    }

    @Override
    public int deleteTravelById(UUID travel_id) {
        Optional<Travel> travelMaybe = selectTravelById(travel_id);

        if(travelMaybe.isEmpty()) {
            return 0;
        }

        DB.remove(travelMaybe.get());

        return 1;
    }

    @Override
    public int updateTravelById(UUID travel_id, Travel updateTravel) {
        return selectTravelById(travel_id)
                .map(travel -> {
                    int indexOfTravelToUpdate = DB.indexOf(travel);

                    if(indexOfTravelToUpdate >= 0) {
                        DB.set(indexOfTravelToUpdate, new Travel(travel_id, updateTravel.getTruckId(), updateTravel.getDriverId(), updateTravel.getPointsTravelled(), updateTravel.getStartPoint(), updateTravel.getCurrentPoint(), updateTravel.getEndPoint()));

                        return 1;
                    }

                    return 0;
                })
                .orElse(0);
    }

}
