package com.example.mantruckmonitor.dao;

import com.example.mantruckmonitor.model.Travel;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TravelDao {
    int insertTravel(UUID travel_id, Travel travel);

    default int insertTravel(Travel travel) {
        UUID travel_id = UUID.randomUUID();

        return insertTravel(travel_id, travel);
    }

    List<Travel> selectAllTravels();

    Optional<Travel> selectTravelById(UUID travel_id);

    int deleteTravelById(UUID travel_id);

    int updateTravelById(UUID travel_id, Travel travel);
}
