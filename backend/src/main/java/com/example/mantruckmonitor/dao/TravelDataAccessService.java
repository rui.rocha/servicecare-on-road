package com.example.mantruckmonitor.dao;

import com.example.mantruckmonitor.model.Travel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("postgresTravelDao")
public class TravelDataAccessService implements TravelDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TravelDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertTravel(UUID travel_id, Travel travel) {
        return 0;
    }

    @Override
    public List<Travel> selectAllTravels() {
        final String sql = "SELECT travel_id, truck_id, driver_id, points_travelled, start_point, current_point, end_point FROM travel";

        return jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID travel_id = UUID.fromString(resultSet.getString(("travel_id")));
            UUID truck_id = UUID.fromString(resultSet.getString(("truck_id")));
            UUID driver_id = UUID.fromString(resultSet.getString(("driver_id")));
            String points_travelled = resultSet.getString("points_travelled");
            String start_point = resultSet.getString("start_point");
            String current_point = resultSet.getString("current_point");
            String end_point = resultSet.getString("end_point");

            return new Travel(travel_id, truck_id, driver_id, points_travelled, start_point, current_point, end_point);
        });
    }

    @Override
    public Optional<Travel> selectTravelById(UUID travel_id) {
        final String sql = "SELECT travel_id, truck_id, driver_id, points_travelled, start_point, current_point, end_point FROM travel WHERE travel_id = ?";

        Travel travel = jdbcTemplate.queryForObject(
                sql,
                new Object[]{travel_id},
                (resultSet, i) -> {
                    UUID travelId = UUID.fromString(resultSet.getString(("travel_id")));
                    UUID truck_id = UUID.fromString(resultSet.getString(("truck_id")));
                    UUID driver_id = UUID.fromString(resultSet.getString(("driver_id")));
                    String points_travelled = resultSet.getString("points_travelled");
                    String start_point = resultSet.getString("start_point");
                    String current_point = resultSet.getString("current_point");
                    String end_point = resultSet.getString("end_point");

                    return new Travel(travelId, truck_id, driver_id, points_travelled, start_point, current_point, end_point);
                });

        return Optional.ofNullable(travel);
    }

    @Override
    public int deleteTravelById(UUID travel_id) {
        return 0;
    }

    @Override
    public int updateTravelById(UUID travel_id, Travel travel) {
        return 0;
    }
}
