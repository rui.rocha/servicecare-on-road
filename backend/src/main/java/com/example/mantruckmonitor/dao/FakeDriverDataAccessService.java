package com.example.mantruckmonitor.dao;

import com.example.mantruckmonitor.model.Driver;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("fakeDriverDao")
public class FakeDriverDataAccessService implements DriverDao {

    private static List<Driver> DB = new ArrayList<>();

    @Override
    public int insertDriver(UUID driver_id, Driver driver) {
        DB.add(new Driver(driver_id, driver.getFirstName(), driver.getLastName(), driver.getPhoneNumber()));

        return 1;
    }

    @Override
    public List<Driver> selectAllDrivers() {
        return DB;
    }

    @Override
    public Optional<Driver> selectDriverById(UUID driver_id) {
        return DB.stream()
                .filter(driver -> driver.getDriverId().equals(driver_id))
                .findFirst();
    }

    @Override
    public int deleteDriverById(UUID driver_id) {
        Optional<Driver> driverMaybe = selectDriverById(driver_id);

        if(driverMaybe.isEmpty()) {
            return 0;
        }

        DB.remove(driverMaybe.get());

        return 1;
    }

    @Override
    public int updateDriverById(UUID driver_id, Driver updateDriver) {
        return selectDriverById(driver_id)
                .map(driver -> {
                    int indexOfDriverToUpdate = DB.indexOf(driver);

                    if(indexOfDriverToUpdate >= 0) {
                        DB.set(indexOfDriverToUpdate, new Driver(driver_id, updateDriver.getFirstName(), updateDriver.getLastName(), updateDriver.getPhoneNumber()));

                        return 1;
                    }

                    return 0;
                })
                .orElse(0);
    }

}
