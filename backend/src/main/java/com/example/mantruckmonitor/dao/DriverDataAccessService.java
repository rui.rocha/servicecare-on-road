package com.example.mantruckmonitor.dao;

import com.example.mantruckmonitor.model.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("postgresDriverDao")
public class DriverDataAccessService implements DriverDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DriverDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertDriver(UUID driver_id, Driver driver) {
        return 0;
    }

    @Override
    public List<Driver> selectAllDrivers() {
        final String sql = "SELECT driver_id, first_name, last_name, phone_number FROM driver";

        return jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID driver_id = UUID.fromString(resultSet.getString(("driver_id")));
            String first_name = resultSet.getString("first_name");
            String last_name = resultSet.getString("last_name");
            String phone_number = resultSet.getString("phone_number");

            return new Driver(driver_id, first_name, last_name, phone_number);
        });
    }

    @Override
    public Optional<Driver> selectDriverById(UUID driver_id) {
        final String sql = "SELECT driver_id, first_name, last_name, phone_number FROM driver WHERE driver_id = ?";

        Driver driver = jdbcTemplate.queryForObject(
                sql,
                new Object[]{driver_id},
                (resultSet, i) -> {
                    UUID driverId = UUID.fromString(resultSet.getString(("driver_id")));
                    String first_name = resultSet.getString("first_name");
                    String last_name = resultSet.getString("last_name");
                    String phone_number = resultSet.getString("phone_number");

                    return new Driver(driverId, first_name, last_name, phone_number);
                });

        return Optional.ofNullable(driver);
    }

    @Override
    public int deleteDriverById(UUID driver_id) {
        return 0;
    }

    @Override
    public int updateDriverById(UUID driver_id, Driver driver) {
        return 0;
    }
}
