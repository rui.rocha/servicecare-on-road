package com.example.mantruckmonitor.dao;

import com.example.mantruckmonitor.model.Truck;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository("fakeDao")
public class FakeTruckDataAccessService implements TruckDao {

    private static List<Truck> DB = new ArrayList<>();

    @Override
    public int insertTruck(UUID truck_id, Truck truck) {
        DB.add(new Truck(truck_id, truck.getPlate()));

        return 1;
    }

    @Override
    public List<Truck> selectAllTrucks() {
        return DB;
    }


}
