package com.example.mantruckmonitor.dao;

import com.example.mantruckmonitor.model.Truck;

import java.util.List;
import java.util.UUID;

public interface TruckDao {
    int insertTruck(UUID truck_id, Truck truck);

    default int insertTruck(Truck truck) {
        UUID truck_id = UUID.randomUUID();

        return insertTruck(truck_id, truck);
    }

    List<Truck> selectAllTrucks();

}
