package com.example.mantruckmonitor.dao;

import com.example.mantruckmonitor.model.Driver;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DriverDao {
    int insertDriver(UUID driver_id, Driver driver);

    default int insertDriver(Driver driver) {
        UUID driver_id = UUID.randomUUID();

        return insertDriver(driver_id, driver);
    }

    List<Driver> selectAllDrivers();

    Optional<Driver> selectDriverById(UUID driver_id);

    int deleteDriverById(UUID driver_id);

    int updateDriverById(UUID driver_id, Driver driver);
}
