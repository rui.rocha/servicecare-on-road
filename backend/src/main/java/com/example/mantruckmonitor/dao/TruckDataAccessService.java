package com.example.mantruckmonitor.dao;

import com.example.mantruckmonitor.model.Truck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("postgres")
public class TruckDataAccessService implements TruckDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TruckDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertTruck(UUID truck_id, Truck truck) {
        return 0;
    }

    @Override
    public int insertTruck(Truck truck) {
        return 0;
    }

    @Override
    public List<Truck> selectAllTrucks() {
        final String sql = "SELECT truck_id, plate FROM truck";

        List<Truck> trucks = jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID truck_id = UUID.fromString(resultSet.getString("truck_id"));
            String plate = resultSet.getString("plate");

            return new Truck(truck_id, plate);
        });

        return trucks;
    }
}
