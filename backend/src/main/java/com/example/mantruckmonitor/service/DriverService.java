package com.example.mantruckmonitor.service;

import com.example.mantruckmonitor.dao.DriverDao;
import com.example.mantruckmonitor.model.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DriverService {

    private final DriverDao driverDao;

    @Autowired
    public DriverService(@Qualifier("postgresDriverDao") DriverDao driverDao) {
        this.driverDao = driverDao;
    }

    public int addDriver(Driver driver) {
        return driverDao.insertDriver(driver);
    }

    public List<Driver> getAllDrivers() {
        return driverDao.selectAllDrivers();
    }

    public Optional<Driver> getDriverById(UUID driver_id) {
        return driverDao.selectDriverById(driver_id);
    }

    public int deleteDriver(UUID driver_id) {
        return driverDao.deleteDriverById(driver_id);
    }

    public int updateDriver(UUID driver_id, Driver newDriver) {
        return driverDao.updateDriverById(driver_id, newDriver);
    }
}
