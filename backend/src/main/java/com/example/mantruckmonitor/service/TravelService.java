package com.example.mantruckmonitor.service;

import com.example.mantruckmonitor.dao.TravelDao;
import com.example.mantruckmonitor.model.Travel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TravelService {

    private final TravelDao travelDao;

    @Autowired
    public TravelService(@Qualifier("postgresTravelDao") TravelDao travelDao) {
        this.travelDao = travelDao;
    }

    public int addTravel(Travel travel) {
        return travelDao.insertTravel(travel);
    }

    public List<Travel> getAllTravels() {
        return travelDao.selectAllTravels();
    }

    public Optional<Travel> getTravelById(UUID travel_id) {
        return travelDao.selectTravelById(travel_id);
    }

    public int deleteTravel(UUID travel_id) {
        return travelDao.deleteTravelById(travel_id);
    }

    public int updateTravel(UUID travel_id, Travel newTravel) {
        return travelDao.updateTravelById(travel_id, newTravel);
    }
}
