package com.example.mantruckmonitor.service;

import com.example.mantruckmonitor.dao.TruckDao;
import com.example.mantruckmonitor.model.Truck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TruckService {

    private final TruckDao truckDao;

    @Autowired
    public TruckService(@Qualifier("postgres") TruckDao truckDao) {
        this.truckDao = truckDao;
    }

    public int addTruck(Truck truck) {
        return truckDao.insertTruck(truck);
    }

    public List<Truck> getAllTrucks() {
        return truckDao.selectAllTrucks();
    }
}
