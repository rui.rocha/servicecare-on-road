package com.example.mantruckmonitor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Driver {
    private final UUID driver_id;
    private final String first_name;
    private final String last_name;
    private final String phone_number;

    public Driver(@JsonProperty("driver_id") UUID driver_id,
                  @JsonProperty("first_name") String first_name,
                  @JsonProperty("last_name") String last_name,
                  @JsonProperty("phone_number") String phone_number) {
        this.driver_id = driver_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone_number = phone_number;
    }

    public UUID getDriverId() {
        return driver_id;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public String getPhoneNumber() {
        return phone_number;
    }
}
