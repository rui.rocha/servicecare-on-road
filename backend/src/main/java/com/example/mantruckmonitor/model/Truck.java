package com.example.mantruckmonitor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Truck {
    private final UUID truck_id;
    private final String plate;

    public Truck(@JsonProperty("truck_id") UUID truck_id,
                 @JsonProperty("plate") String plate) {

        this.truck_id = truck_id;
        this.plate = plate;
    }

    public UUID getId() {
        return truck_id;
    }

    public String getPlate() {
        return plate;
    }
}
