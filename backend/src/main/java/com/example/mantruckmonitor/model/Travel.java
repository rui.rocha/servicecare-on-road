package com.example.mantruckmonitor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.UUID;

public class Travel {
    private final UUID travel_id;
    private final UUID truck_id;
    private final UUID driver_id;
    private final String points_travelled;
    private final String start_point;
    private final String current_point;
    private final String end_point;

    public Travel(@JsonProperty("travel_id") UUID travel_id,
                  @JsonProperty("travel_id") UUID truck_id,
                  @JsonProperty("travel_id") UUID driver_id,
                  @JsonProperty("points_travelled") String points_travelled,
                  @JsonProperty("start_point") String start_point,
                  @JsonProperty("current_point") String current_point,
                  @JsonProperty("end_point") String end_point) {

        this.travel_id = travel_id;
        this.truck_id = truck_id;
        this.driver_id = driver_id;
        this.points_travelled = points_travelled;
        this.start_point = start_point;
        this.current_point = current_point;
        this.end_point = end_point;
    }

    public UUID getTravelId() {
        return travel_id;
    }

    public UUID getTruckId() {
        return truck_id;
    }

    public UUID getDriverId() {
        return driver_id;
    }

    public String getPointsTravelled() {
        return points_travelled;
    }

    public String getStartPoint() {
        return start_point;
    }

    public String getCurrentPoint() {
        return current_point;
    }

    public String getEndPoint() {
        return end_point;
    }
}
