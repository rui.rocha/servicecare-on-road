package com.example.mantruckmonitor.api;

import com.example.mantruckmonitor.model.Driver;
import com.example.mantruckmonitor.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping("api/v1/driver")
@RestController
public class DriverController {

    private final DriverService driverService;

    @Autowired
    public DriverController(DriverService driverService) {
        this.driverService = driverService;
    }

    @PostMapping
    public void addDriver(@RequestBody Driver driver) {
        driverService.addDriver(driver);
    }

    @GetMapping
    public List<Driver> getAllDrivers() {
        return driverService.getAllDrivers();
    }

    @GetMapping(path = "{driver_id}")
    public Driver getDriverById(@PathVariable("driver_id") UUID driver_id) {
        return driverService.getDriverById(driver_id)
                .orElse(null);
    }

    @DeleteMapping(path = "{driver_id}")
    public void deleteDriverById(@PathVariable("driver_id") UUID driver_id) {
        driverService.deleteDriver(driver_id);
    }

    @PutMapping(path = "{driver_id}")
    public void updateDriverById(@PathVariable("driver_id") UUID driver_id, @RequestBody Driver driverToUpdate) {
        driverService.updateDriver(driver_id, driverToUpdate);
    }
}
