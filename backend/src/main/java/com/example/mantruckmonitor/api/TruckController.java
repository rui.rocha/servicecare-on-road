package com.example.mantruckmonitor.api;

import com.example.mantruckmonitor.model.Truck;
import com.example.mantruckmonitor.service.TruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/v1/truck")
@RestController
public class TruckController {

    private final TruckService truckService;

    @Autowired
    public TruckController(TruckService truckService) {
        this.truckService = truckService;
    }

    @PostMapping
    public void addTruck(@RequestBody Truck truck) {
        truckService.addTruck(truck);
    }

    @GetMapping
    public List<Truck> getAllTrucks() {
        return truckService.getAllTrucks();
    }
}
