package com.example.mantruckmonitor.api;

import com.example.mantruckmonitor.model.Travel;
import com.example.mantruckmonitor.service.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping("api/v1/travel")
@RestController
public class TravelController {

    private final TravelService travelService;

    @Autowired
    public TravelController(TravelService travelService) {
        this.travelService = travelService;
    }

    @PostMapping
    public void addTravel(@RequestBody Travel travel) {
        travelService.addTravel(travel);
    }

    @GetMapping
    public List<Travel> getAllTravels() {
        return travelService.getAllTravels();
    }

    @GetMapping(path = "{travel_id}")
    public Travel getTravelById(@PathVariable("travel_id") UUID travel_id) {
        return travelService.getTravelById(travel_id)
                .orElse(null);
    }

    @DeleteMapping(path = "{travel_id}")
    public void deleteTravelById(@PathVariable("travel_id") UUID travel_id) {
        travelService.deleteTravel(travel_id);
    }

    @PutMapping(path = "{travel_id}")
    public void updateTravelById(@PathVariable("travel_id") UUID travel_id, @RequestBody Travel travelToUpdate) {
        travelService.updateTravel(travel_id, travelToUpdate);
    }
}
