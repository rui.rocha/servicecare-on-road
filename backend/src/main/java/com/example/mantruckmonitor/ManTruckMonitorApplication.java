package com.example.mantruckmonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManTruckMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManTruckMonitorApplication.class, args);
	}

}
