CREATE TABLE IF NOT EXISTS travel (
    travel_id UUID NOT NULL PRIMARY KEY,
    truck_id UUID NOT NULL REFERENCES truck (truck_id),
    driver_id UUID NOT NULL REFERENCES driver (driver_id),
    points_travelled POINT[],
    start_point POINT NOT NULL,
    current_point POINT NOT NULL,
    end_point POINT NOT NULL,
    UNIQUE (truck_id, driver_id)
);
